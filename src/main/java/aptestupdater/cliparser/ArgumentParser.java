package aptestupdater.cliparser;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;

public class ArgumentParser {
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String SUITE = "suite";
	private static final String RUN_NUMBER = "run";
	private static final String XML_LOCATION = "xml";
	private static final String HEADLESS = "headless";
	private static final String SLOWPOKE = "andale";
	private static final String BROWSER_TYPE = "browser type";
	
	private SimpleJSAP jsap; 
	private JSAPResult result;
	
	public ArgumentParser() throws Exception{
		jsap = new SimpleJSAP( 
	            "java -jar aptestUpdater.jar", 
	            "Batch ApTest updater. Reads the specified jUnit XML output file and updates the specified suite and run in the ApTest server."
	        );
		
		FlaggedOption username = new FlaggedOption(USERNAME).
				setStringParser(JSAP.STRING_PARSER).
				setRequired(true).
				setShortFlag('u');
		username.setHelp("username used to login to ApTest");
		jsap.registerParameter(username);
		
		FlaggedOption password = new FlaggedOption(PASSWORD).
				setStringParser(JSAP.STRING_PARSER).
				setRequired(true).
				setShortFlag('p');
		password.setHelp("password used to login to ApTest");
		jsap.registerParameter(password);
		
		FlaggedOption suite = new FlaggedOption(SUITE).
				setStringParser(JSAP.STRING_PARSER).
				setRequired(true).
				setShortFlag('s');
		suite.setHelp("suite to be updated (i.e. Automation)");
		jsap.registerParameter(suite);
		
		FlaggedOption runNumber = new FlaggedOption(RUN_NUMBER).
			setStringParser(JSAP.STRING_PARSER).
			setRequired(true).
			setShortFlag('r');
		runNumber.setHelp("run number to be updated (i.e. 87)");
		jsap.registerParameter(runNumber);
		
		FlaggedOption browserType = new FlaggedOption(BROWSER_TYPE).
				setStringParser(JSAP.STRING_PARSER).
				setRequired(true).
				setDefault("Firefox").
				setShortFlag('b');
			browserType.setHelp("browser type: Firefox, Chrome, IE, Safari");
			jsap.registerParameter(browserType);
		
		Switch runHeadless = new Switch(HEADLESS).
				setShortFlag('h');
		runHeadless.setHelp("run this application with a headless browser");
		jsap.registerParameter(runHeadless);
		
		Switch slowpoke = new Switch(SLOWPOKE).
				setShortFlag('z');
		jsap.registerParameter(slowpoke);
		
		UnflaggedOption xmlLocation = new UnflaggedOption(XML_LOCATION).
			setStringParser(JSAP.STRING_PARSER).
			setRequired(true);
		xmlLocation.setHelp("path to the jUnit xml file containing the new test results");
		jsap.registerParameter(xmlLocation);
	
	}
	
	public void parse(String[] args){
		result = jsap.parse(args);
		if(!result.success()){
			System.err.println("Usage: java -jar aptestUpdater.jar ");
			System.err.println("                "
                    + jsap.getUsage());
		}
	}
	
	public String getUsername(){
		return result.getString(USERNAME);
	}
	
	public String getPassword(){
		return result.getString(PASSWORD);
	}

	public String getSuite(){
		return result.getString(SUITE);
	}
	
	public String getRunNumber(){
		String aptNumber = result.getString(RUN_NUMBER);
		return String.format("%0"+(6-aptNumber.length())+"d%s", 0, aptNumber);
	}
	
	public String getXmlLocation(){
		return result.getString(XML_LOCATION).replace("\\", "/");
	}
	
	public boolean isLegacyRunRequested(){
		return result.getBoolean(SLOWPOKE);
	}

	public String getBrowserType() {
		return result.getString(BROWSER_TYPE);
	}
}