package aptestupdater.jaxb;

import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class TestResultUnmarshaller {
	
	public static List<TestCase> getJunitTestCaseResults(File junitFile){
		try {
			 
			File file = junitFile;
			JAXBContext jaxbContext = JAXBContext.newInstance(TestSuite.class);
	 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			TestSuite testSuite = (TestSuite) jaxbUnmarshaller.unmarshal(file);
			
			List<TestCase> testCases = testSuite.getTestCases();
			
			return testCases;
	 
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
		
		return null;
	}
	
	
}