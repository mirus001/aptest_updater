package aptestupdater.jaxb;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class TestCase {

	private String name;
	private String time;
	private String classname;
	private String failure;
	private String error;
	
	public String toString(){
		return "name: "+name+", time: "+time+", classname: "+getClassName()+", failure: "+failure+" error: "+getTruncatedError(5);
		
	}
	
	@XmlAttribute
	public void setName(String name){
		this.name = name;
	}
	
	@XmlAttribute
	public void setTime(String time){
		this.time = time;
	}
	
	@XmlAttribute
	public void setClassname(String classname){
		this.classname = classname;
	}
	
	@XmlElement(name="error")
	public void setError(String error){
		this.error = error.replaceAll("\r\n", "\n");
		this.error = this.error.replaceAll("\t", "  ");
	}
	
	@XmlElement(name="failure")
	public void setFailure(String failure){
		this.failure = failure;
	}
	
	public String getMethodName(){
		return name;
	}
	
	public String getTime(){
		return time;
	}
	
	public String getClassNameWithPath(){
		return classname;
	}
	
	public boolean isTestPassed(){
		return error == null && failure == null;
	}
	
	public String getClassName(){
		String cleanName = classname;
		int startIndex = classname.lastIndexOf('.')+1;
		return cleanName.substring(startIndex);
	}
	
	public String getFailure(){
		return failure;
	}
	
	public String getError(){
		return error;
	}
	
	public String getTruncatedError(int linesToInclude){
		return getTruncatedString(linesToInclude, error);
	}
	
	public String getTruncatedFailure(int linesToInclude){
		return getTruncatedString(linesToInclude, failure);
	}
	
	private String getTruncatedString(int linesToInclude, String message){
		if(message == null){
			return message;
		}
		
		// tmpString will be used to store intermediate strings
		String tmpString = message;
		// the cutoffIndex will be the position of the nth \n
		int cutoffIndex = 0;
		int newlineCount = 0;
		// loop exit criteria
		boolean limitReached = false;
		
		while(!limitReached){
			// no new line? exit
			if(!tmpString.contains("\n") || (newlineCount >= linesToInclude)){
				limitReached = true;
			} else {
				int index =  tmpString.indexOf("\n");
				if(index >= 0){
					tmpString = tmpString.substring(index+1);
					cutoffIndex += index;
					newlineCount++;
				} else {
					limitReached = true;
				}
			}
		}
		return error.substring(0, cutoffIndex)+"\n<truncated...>";
	}

	public String getJiraUrls() {
		return null;
	}
}