package aptestupdater.jaxb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="testrun")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestSuite {
     
	@XmlElementWrapper(name="testsuite")
	@XmlElement(name="testcase")
    private List<TestCase> testCases;
	
	public List<TestCase> getTestCases(){
		return testCases;
	}
	
	public void setTestCases(List<TestCase> testCases){
		this.testCases = testCases;
	}
}