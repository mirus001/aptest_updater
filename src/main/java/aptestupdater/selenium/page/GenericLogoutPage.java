package aptestupdater.selenium.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class GenericLogoutPage extends PageObject{

	private static final String XPATH_LOGOUT_LINK = "//*[@href=\"/atm/accounts/logout.mpl\"]";

	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded = waitUntilPageObjectContains(XPATH, XPATH_LOGOUT_LINK);
		if(domLoaded){
			super.saveCurrentWindowHandle();
		}
		return domLoaded;
	}

	public boolean isCurrentPage() {
		return contains(XPATH, XPATH_LOGOUT_LINK);
	}
	
	public void logout(){
		WebElement logoutLink = driver.findElement(By.xpath(XPATH_LOGOUT_LINK));
		logoutLink.click();
	}

}