package aptestupdater.selenium.page;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * An abstract representation of a page object with common methods to be used by all page object classes
 * @author Mirus Lu
 *
 */
public abstract class PageObject{
	// The webdriver and system under test
	protected static RemoteWebDriver driver;
	protected static String systemUnderTestUrl;
	private static final String searchByTextXpath = "//*[contains(., \"\")]";
	
	// Parameters that define search parameters
	protected final int XPATH = 0;
	protected final int TEXT = 1;
	protected final int URL_FULL = 2;
	protected final int URL_PARTIAL = 3;
	
	// Parameters used in search and wait methods
	private final long pollingIntervalMillis = 500;
	private final long defaultPollingTimeout = 3000;
	
	private String windowHandle = null;
	
	/**
	 * Sets the webdriver associated with all page objects. Only one page object is active at any time
	 * @param webDriver the WebDriver object
	 */
	public static void setWebDriver(RemoteWebDriver webDriver) {
		driver = webDriver;
	}
	
	/**
	 * Calls PageFactory with the default WebDrriver associated with this page object
	 */
	public void populateElements(){
		populateElements(driver);
	}
	
	/**
	 * Calls PageFactory with the specified WebDriver object
	 * @param newDriver the WebDriver instance used to populate the elements
	 */
	public void populateElements(RemoteWebDriver newDriver){
		driver = newDriver;
		PageFactory.initElements(newDriver, this);
	}
	
	/**
	 * Reloads the current page.
	 */
	public void reloadPage(){
		driver.navigate().refresh();
	}
	
	/**
	 * Go to the url specified
	 * @param url
	 */
	public static void goToUrl(String url){
		driver.get(url);
	}
	
	public static String getCurrentUrl(){
		return driver.getCurrentUrl();
	}

	/**
	 * Retrieve the WebDriver object associated with the page object
	 * @return the WebDriver object
	 */
	public static RemoteWebDriver getWebDriver(){
		return driver;
	}
	
	/**
	 * Set the url of the system under test
	 * @param theSystemUnderTestUrl a url string
	 */
	public static void setBaseUrl(String theSystemUnderTestUrl) {
		systemUnderTestUrl = theSystemUnderTestUrl;
	}

	/**
	 * Retrieve the system under test's url
	 * @return a url String
	 */
	public String getSystemUnderTestUrl(){
		return systemUnderTestUrl;
	}
	
	/**
	 * Wait until the page contains the given search term
	 * @param type can be XPATH, TEXT, URL_FULL, URL_PARTIAL
	 * @param value a string representing the search value
	 * @return true if the element was found
	 */
	protected boolean waitUntilPageObjectContains(int type, String value){
		return waitUntilPageObjectContains(type, value, defaultPollingTimeout);
	}
	
	protected boolean waitUntilElementIsDisplayed(WebElement element, long timeoutMilliseconds){
		int seconds = (int)TimeUnit.MILLISECONDS.toSeconds(timeoutMilliseconds);
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		try{
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e){
		}
		return element.isDisplayed();
	}
	
	protected boolean waitUntilElementIsDisplayed(String xpath, long timeoutMilliseconds) {
		
		return waitUntilElementIsDisplayed(driver.findElement(By.xpath(xpath)), timeoutMilliseconds);
	}
	protected boolean isElementDisplayed(int type, String value){
		return findElement(type, value).isDisplayed();
	}
	
	protected WebElement findElement(int type, String value){
		WebElement webElement = null;
		switch(type){
		case XPATH:
			try{
				webElement = driver.findElement(By.xpath(value));
			} catch (Exception e) {
				// do nothing. If flow gets into this, it means that webdriver hasn't found the element
			}
			break;
		case TEXT:
			try{
				StringBuffer buffer = new StringBuffer(searchByTextXpath);
				String xpath = buffer.insert(searchByTextXpath.length()-3, value).toString();
				webElement = driver.findElement(By.xpath(xpath));
			} catch (Exception e) {
				// do nothing. If flow gets into this, it means that webdriver hasn't found the element
			}
			break;
		}
		
		return webElement;
	}
	
	/**
	 * Determines if the page contains the specified value
	 * @param type can be XPATH, TEXT, URL_FULL, URL_PARTIAL
	 * @param value a string representing the search value
	 * @return true if the element was found
	 */
	protected boolean contains(int type, String value){
		return findElement(type, value) != null;
	}
	
	/**
	 * Waits until the specified search value is found or until the timeout expires
	 * @param type can be XPATH, TEXT, URL_FULL, URL_PARTIAL
	 * @param value a string representing the search value
	 * @param timeout time in milliseconds to search for the term
	 * @return true if the element was found on the page within the timeout period
	 */
	protected boolean waitUntilPageObjectContains(int type, String value, long timeout){
		WebDriverWait wait = new WebDriverWait(driver, 5);
		boolean elementFound = false;
		long startTime = System.currentTimeMillis();
		elementFound = contains(type, value);
		
		while (System.currentTimeMillis()-startTime < timeout && !elementFound){
			if(type == XPATH){
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value)));
			} else {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(.,\""+value+"\")]")));
			}
			
			elementFound = contains(type, value);
		} 
		
		return elementFound;
	}
	
	public void scrollToWebElement(WebElement element) {
		// this call is enough to bring the element into the viewport in Appium
		element.getLocation();
	}
	
	
	public int getBrowserTabCount(){
		return driver.getWindowHandles().size();
	}
	
	public String getCurrentWindowHandle(){
		return driver.getWindowHandle();
	}
	
	public void intentionalWait(long timeout){
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
		}
	}
	
	/**
	 * The inherited class will need to implement this to reflect when the page's dom is loaded 
	 * @return true if the dom was loaded within the time limit
	 */
	public abstract boolean waitUntilDomIsLoaded();
	
	/**
	 * Determine whether the current page on screen is the current page
	 * @return true if the current page is indeed this page object
	 */
	public abstract boolean isCurrentPage();

	
	protected String getWindowHandleOfNewPopup(Set<String> initialWindowHandles) {
		return getWindowHandleOfNewPopup(initialWindowHandles, 60000);
	}
	
	protected String getWindowHandleOfNewPopup(Set<String> initialWindowHandles, long timeout) {
		long startTime = System.currentTimeMillis();
		boolean popupFound = false;
		String popupHandle = null;
		Set<String> handlesNow = null;
		
		// wait for a new handle to appear
		while(System.currentTimeMillis()-startTime < timeout && !popupFound){
			// update the handlesNow on each loop
			handlesNow = driver.getWindowHandles();
			
			// determine if there's a new popup handles
			popupHandle = findNewWindowHandle(handlesNow, initialWindowHandles);
			if(popupHandle != null){
				popupFound = true;
			} else {
				intentionalWait(500);
			}
		}
		
		return popupHandle;
		
	}
	
	private String findNewWindowHandle(Set<String> newHandleSet, Set<String> oldHandleSet){
		Iterator<String> currentHandleIterator = newHandleSet.iterator();
		Iterator<String> originalHandleIterator = oldHandleSet.iterator();
		boolean uniqueHandleFound = false;
		boolean matchFound = false;
		String currentHandle, originalHandle;
		String popupHandle = null;
		
		// compare all new handles against original to try to find a match. If no match is found
		// for a new handle, then the new handle was a popup
		while(currentHandleIterator.hasNext() && !uniqueHandleFound){
			// for each current handle...
			currentHandle = currentHandleIterator.next();
			
			// compare it against each original handle...
			while(originalHandleIterator.hasNext() && !matchFound){
				originalHandle = (String)originalHandleIterator.next();
				
				// if we found a matching handle, then this wasn't a popup...
				if(currentHandle.equals(originalHandle)){
					matchFound = true;
				}
			}
			
			//... match found, skip to the next currentHandle
			if(matchFound){
				matchFound = false;
				uniqueHandleFound = false;
			} else {
				// ... no matches found? Then this new handle is a popup
				uniqueHandleFound = true;
				popupHandle = currentHandle;
			}
		}
		
		
		return popupHandle;
		
	}
	
	protected void saveCurrentWindowHandle(){
		windowHandle = driver.getWindowHandle();
	}
	
	
	public void setWindowFocus(){
		driver.switchTo().window(windowHandle);
	}
}