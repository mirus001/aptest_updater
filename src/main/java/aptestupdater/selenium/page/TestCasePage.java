package aptestupdater.selenium.page;

import aptestupdater.jaxb.TestCase;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.support.ui.Select;

public class TestCasePage extends PageObject {

	private static final String PASS_MESSAGE = "Test passed in Eclipse on ";
	private static final String EXECUTION_TIME_MESSAGE = "Execution time was ";
	private static final String FAIL_MESSAGE = "Test failed in Eclipse on ";
	private static final String FAILURE_MESSAGE = "Stack trace was: ";
	private static final String TEST_FAILED_WITH_ERROR_MESSAGE = "Test failed with exception:";
	private static final String TEST_FAILED_MESSAGE = "Test failed:";
	
	private static final String NOTE_FIELD_XPATH = "//textarea[@name='note']";
	private static final String SEND_RESULT_XPATH = "//input[@value='Send Result']";
	private static final String STAFF_TIME_SELECTOR_PATH = "//select[@name='RUNDATA_exectimestaff']";
	private static final String PROBLEM_REPORT_LINKS_PATH = "//select[@name='RUNDATA_atm_prlink']";
	
	private static final String RESULT_SELECTOR_XPATH = "//select[@name='results']";
	private static final String OPTION_PASSED = "pass - The test passed";
	private static final String OPTION_FAILED = "fail - The test failed";
	
	private final String DEFAULT_STAFF_TIME = "2";
	
	private WebElement noteField;
	private Select resultSelector;
	private Select staffTimeSelector;
	private WebElement sendResultButton;
	private WebElement problemReportLinksField;
	
	public void fillTestResultFields(TestCase caseResult) {
		// retrieve all information from the testcase object
		String testClass = caseResult.getClassName();
		String testMethod = caseResult.getMethodName();
		String runTimeInSeconds = caseResult.getTime();
		String error = caseResult.getTruncatedError(8);
		String failure = caseResult.getTruncatedFailure(8);
		String jiraLinks = caseResult.getJiraUrls();
		boolean testPassed = caseResult.isTestPassed();
		
		// get the current date
		Date updaterLaunchTime = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String dateString = dateFormat.format(updaterLaunchTime);
		
		// prep a message to enter into the notes field
		String message = "";
		
		// if the test passed, set the Result dropdown to "pass - The test passed", otherwise set it to "fail - The test failed"
		Select resultSelector = new Select(driver.findElement(By.xpath(RESULT_SELECTOR_XPATH)));
		if(testPassed){
			resultSelector.selectByVisibleText(OPTION_PASSED);
			message = PASS_MESSAGE;
		} else {
			resultSelector.selectByVisibleText(OPTION_FAILED);
			message = FAIL_MESSAGE;
		}
		
		// fill in the rest of the message
		message = message+dateString+"\n"+EXECUTION_TIME_MESSAGE+runTimeInSeconds+"s.\n";
		
		if(error != null){
			message = message+TEST_FAILED_WITH_ERROR_MESSAGE+"\n"+error;
		}
		
		if(failure != null){
			message = message+TEST_FAILED_MESSAGE+"\n"+failure;
		}
		
		if(jiraLinks != null){
			setProblemReportLinks(jiraLinks);
		}
		
		// set the Actual Staff Time
		selectStaffTimeByVisibleText(DEFAULT_STAFF_TIME);
		
		// fill in the execution notes field
		setNote(message);
		
	}
	
	public void selectStaffTimeByVisibleText(String text){
		// fetch the elements
		staffTimeSelector = new Select(driver.findElement(By.xpath(STAFF_TIME_SELECTOR_PATH)));
		staffTimeSelector.selectByVisibleText(text);
	}
	
	public void setNote(String note){
		noteField = driver.findElement(By.xpath(NOTE_FIELD_XPATH));
		noteField.sendKeys(note);
	}
	
	public void clickSendResultButton(){
		sendResultButton = driver.findElement(By.xpath(SEND_RESULT_XPATH));
		sendResultButton.click();
		
		
	}
	
	public void closeAlert(){
		if(waitForAlert(2000)){
			confirmAlert();
		}
	}
	private boolean waitForAlert(long timeoutInMilliseconds){
		long startTime = System.currentTimeMillis();
		boolean alertShown = false;
		while(!alertShown && System.currentTimeMillis()-startTime<timeoutInMilliseconds){
			try{
				Alert alert = driver.switchTo().alert();
				return true;
			} catch (Exception e) {
				
			}
		}
		
		return false;
	}
	
	private void confirmAlert() {
		try{
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e){
		}
	}
	
	public void setProblemReportLinks(String links){
		problemReportLinksField = driver.findElement(By.xpath(PROBLEM_REPORT_LINKS_PATH));
		problemReportLinksField.sendKeys(links);
	}
	
	@Override
	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded =  waitUntilPageObjectContains(XPATH, RESULT_SELECTOR_XPATH);
		if(domLoaded){
			saveCurrentWindowHandle();
		}
		
		return domLoaded;
	}

	@Override
	public boolean isCurrentPage() {
		return isElementDisplayed(XPATH, SEND_RESULT_XPATH);
	}

}