package aptestupdater.selenium.page;

public class SelectAnOperationPage extends PageObject {
	private static final String SELECT_AN_OPERATION_TEXT = "Select an Operation";
	
	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded = waitUntilPageObjectContains(TEXT, SELECT_AN_OPERATION_TEXT);
		if(domLoaded){
			super.saveCurrentWindowHandle();
		}
		return domLoaded;
	}

	public boolean isCurrentPage() {
		return driver.getPageSource().contains(SELECT_AN_OPERATION_TEXT);
	}

}