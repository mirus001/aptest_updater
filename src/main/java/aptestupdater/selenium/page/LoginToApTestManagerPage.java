package aptestupdater.selenium.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginToApTestManagerPage extends PageObject{

	public static final String USERNAME_FIELD = "//input[@type='text']";
	public static final String PASSWORD_FIELD = "//input[@type='password']";
	public static final String LOGIN_BUTTON  = "//input[@value='Login']";
	public static final String TEXT_LOGIN_PROMPT = "Please enter your user name and password";
	public static final String LOGIN_PROMPT = "//p[contains(text(), \""+TEXT_LOGIN_PROMPT+"\")]";
	public static final String ERROR_IMAGE = "//img[@src='/atm/images/fixed/error.gif']";
	public static final String ERROR_MESSAGE_TD = "//tr//td[img[@src='/atm/images/fixed/error.gif']]/following-sibling::td[1]";
	
	@FindBy(how = How.XPATH, using = USERNAME_FIELD)
	WebElement usernameField;
	
	@FindBy(how = How.XPATH, using = PASSWORD_FIELD)
	WebElement passwordField;
	
	@FindBy(how = How.XPATH, using = LOGIN_BUTTON)
	WebElement loginButton;
	
	public void setUsername(String username){
		usernameField.sendKeys(username);
	}
	
	public void setPassword(String password){
		passwordField.sendKeys(password);
	}
	
	public void clickLoginButton(){
		loginButton.click();
	}
	
	
	public void login(String username, String password){
		setUsername(username);
		setPassword(password);
		clickLoginButton();
	}
	
	public boolean isErrorMessagePresent(){
		return isElementDisplayed(XPATH, ERROR_IMAGE);
	}
	
	public String getErrorMessage(){
		if(isErrorMessagePresent()){
			WebElement errorRow = driver.findElement(By.xpath(ERROR_MESSAGE_TD));
			return errorRow.getText();
		}
		return null;
	}

	@Override
	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded = waitUntilPageObjectContains(XPATH, LOGIN_BUTTON);
		if(domLoaded){
			super.saveCurrentWindowHandle();
		}
		return domLoaded;
	}

	@Override
	public boolean isCurrentPage() {
		return waitUntilPageObjectContains(XPATH, LOGIN_PROMPT);
	}
	
}