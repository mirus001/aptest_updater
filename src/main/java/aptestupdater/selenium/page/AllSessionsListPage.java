package aptestupdater.selenium.page;

import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AllSessionsListPage extends PageObject {

	public static final String ATM_RESULT_TABLE = "//table[@class='atm_dataTable']";
	
	/**
	 * Clicks on the run number and returns the window handle of the popup that contains the run details
	 * 
	 * This currently isn't implemented for run numbers that aren't on the first page.
	 * @param runNumber an integer representing the run index
	 * @return a String representing the popup window that was spawned as a result of clicking on this window
	 */
	public void clickOnRunNumber(String runNumber){
		String xpathExpression = "//a[contains(@href, \""+runNumber+"\") and contains(@href, \"sessionSummary\")]";
		// click on the run selection then wait for the popup window that contains the methods that were executed in the run
		Set<String> handlesBeforePopup = driver.getWindowHandles();
		WebElement runSelectorLink = driver.findElement(By.xpath(xpathExpression));
		
		
		runSelectorLink.click();
		String popupWindow = getWindowHandleOfNewPopup(handlesBeforePopup, 5000);
		driver.switchTo().window(popupWindow);
	}

	@Override
	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded = waitUntilPageObjectContains(XPATH, ATM_RESULT_TABLE);
		if(domLoaded){
			super.saveCurrentWindowHandle();
		}
		return domLoaded;
	}

	@Override
	public boolean isCurrentPage() {
		return isElementDisplayed(XPATH, ATM_RESULT_TABLE);
	}

}