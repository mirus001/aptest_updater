package aptestupdater.selenium.page;

import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SummaryOfSessionPage extends PageObject{

	public static enum TestResult {
		PASS, FAIL, UNTESTED, TEST_NOT_FOUND;
	}
	
	public static final String PAGE_TITLE = "//div[@id='pageTitle' and contains(text(), \"Summary of Session\")]";
	private static final String XPATH_SUFFIX_TEST_PASSED = "//font[contains(text(), \"pass\")]"; 
	private static final String XPATH_SUFFIX_TEST_FAILED = "//font[contains(text(), \"fail\")]";
	private static final String XPATH_SUFFIX_TEST_UNTESTED = "//font[contains(text(), \"untested\")]"; 
	
	private static final String XPATH_SUFFIX_RUN_TEST_LINK = "//a[contains(@href, 'javascript:runTestCase')]";
	
	@Override
	public boolean waitUntilDomIsLoaded() {
		boolean domLoaded = waitUntilPageObjectContains(XPATH, PAGE_TITLE);
		if(domLoaded){
			super.saveCurrentWindowHandle();
		}
		return domLoaded;
	}

	@Override
	public boolean isCurrentPage() {
		return isElementDisplayed(XPATH, PAGE_TITLE);
	}
	

	public void clickRunThisTest(String className, String methodName) {
		boolean testFound = goToResultPageWithTest(className, methodName);
		
		if(testFound){
			// this is the row that contains the links related to the test case that was run
			String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
			String runXpath = testCaseRowXpath+XPATH_SUFFIX_RUN_TEST_LINK;
			WebElement runThisTestLink = driver.findElement(By.xpath(runXpath));
			Set<String> handlesBeforePopup = driver.getWindowHandles();
			runThisTestLink.click();
			
			// switch to the popup with the test case editor fields and fill them in.
			String testEditorHandle = getWindowHandleOfNewPopup(handlesBeforePopup);
			driver.switchTo().window(testEditorHandle);
		}
	}
	
	public TestResult getTestResult(String className, String methodName){
		boolean testFound = goToResultPageWithTest(className, methodName);
		TestResult returnValue = TestResult.TEST_NOT_FOUND;
		if(!testFound){
			returnValue = TestResult.TEST_NOT_FOUND;
		} else {
			// this is the row that contains the links related to the test case that was run
			String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
			// this is the xpath to the column that contains 'fail' text in it
			String testCaseFailXpath = testCaseRowXpath+XPATH_SUFFIX_TEST_FAILED;
			
			waitUntilPageObjectContains(XPATH, testCaseRowXpath);
			WebElement row = driver.findElement(By.xpath(testCaseRowXpath));
			try{
				WebElement status = row.findElement(By.xpath(".//td[4]"));
				if(status.getText().contains("fail")){
					returnValue = TestResult.FAIL;
				} else if(status.getText().contains("pass")) {
					returnValue = TestResult.PASS;
				} else if(status.getText().contains("untested")) {
					returnValue = TestResult.UNTESTED;
				} else {
					returnValue = TestResult.TEST_NOT_FOUND;
				}
			} catch(Exception e){
				returnValue = TestResult.TEST_NOT_FOUND;
			}	
		}
		return returnValue;
	}
	
	
	
	private boolean goToResultPageWithTest(String className, String methodName){
		int pageCount = getPageCount();
		int currentPage = getCurrentPage();
		int pagesSearched = 0;

		
		// this is the row that contains the links related to the test case that was run
		String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
		boolean matchFound = false;
		while(!matchFound && (pagesSearched < pageCount)){
			// search the current page for the test case entry, only if it failed
			boolean linkFound = waitUntilPageObjectContains(XPATH, testCaseRowXpath);
			if(linkFound){
				WebElement testCaseLink = driver.findElement(By.xpath(testCaseRowXpath));
				String runXpath = testCaseRowXpath+XPATH_SUFFIX_RUN_TEST_LINK;
				//WebElement runThisTestLink = driver.findElement(By.xpath(runXpath));
				matchFound = true;
			} else {
				// not on the current page, check the next page
				currentPage = currentPage > pageCount ? currentPage = 1 : currentPage+1;
				pagesSearched++;
				WebElement pageBox = driver.findElement(By.xpath("//input[@name = 'botPage']"));
				pageBox.sendKeys(Integer.toString(currentPage));
				pageBox.submit();
				waitUntilPageObjectContains(TEXT, currentPage+" of "+pageCount); 
			}
		}
		
		return matchFound;
	}

	private int getPageCount(){
		String header = "Display of page";
		waitUntilPageObjectContains(TEXT, header);
		try{
		WebElement counter = driver.findElement(By.xpath("//p[contains(text(), \""+header+"\")]"));
		String displayCount = counter.getText();
		
		displayCount = displayCount.substring(0, displayCount.lastIndexOf("."));
		String maxStr = displayCount.substring(displayCount.lastIndexOf(" ")).trim();
		return Integer.parseInt(maxStr);
		} catch (Exception e) {
			System.err.println(driver.getPageSource());
		}
		
		return -1;
	}
	
	private int getCurrentPage(){
		String header = "Display of page";
		
		WebElement counter = driver.findElement(By.xpath("//p[contains(text(), \""+header+"\")]"));
		String displayCount = counter.getText();
		
		String currentPage = displayCount.substring(0, displayCount.lastIndexOf(" of "));
		currentPage = currentPage.substring(currentPage.lastIndexOf(" ")).trim();
		
		return Integer.parseInt(currentPage); 
		
	}
	
	public void closeBrowser(){
		setWindowFocus();
		driver.close();
	}
}