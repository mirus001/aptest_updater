package aptestupdater.selenium;

import aptestupdater.jaxb.TestCase;
import aptestupdater.jaxb.TestResultUnmarshaller;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Command;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.ErrorHandler.UnknownServerException;
import org.openqa.selenium.support.ui.Select;

public class Updater {

	WebDriver driver;
	public static final String BASE_URL = "http://172.16.10.114/atm";
	public static final int HTML_UNIT_DRIVER = 0;
	public static final int FIREFOX_DRIVER = 1;
	public static final int IE_DRIVER = 2;
	
	private static final long ONE_SECOND = 1000;
	private static final long THIRTY_SECONDS = 30 * ONE_SECOND;
	private static final long ONE_MINUTE = 60 * ONE_SECOND;
	private static final long FIVE_MINUTES = 5 * ONE_MINUTE;
	
	private static final int XPATH = 0;
	private static final int TEXT = 1;
	
	private int driverType = HTML_UNIT_DRIVER;
	
	private String username;
	private String password;
	
	private static final String TEXT_LOGIN_PROMPT = "Please enter your user name and password";
	private static final String XPATH_LOGOUT_LINK = "//*[@href=\"/atm/accounts/logout.mpl\"]";
	private static final String XPATH_SUFFIX_TEST_PASSED = "//font[contains(text(), \"pass\")]"; 
	private static final String XPATH_SUFFIX_TEST_FAILED = "//font[contains(text(), \"fail\")]";
	private static final String XPATH_SUFFIX_RUN_TEST_LINK = "//a[contains(@href, 'javascript:runTestCase')]";
	
	private static final String PASS_MESSAGE = "Test passed in Eclipse on ";
	private static final String EXECUTION_TIME_MESSAGE = "Execution time was ";
	private static final String FAIL_MESSAGE = "Test failed in Eclipse on ";
	private static final String FAILURE_MESSAGE = "Stack trace was: ";
	private static final String TEST_FAILED_WITH_ERROR_MESSAGE = "Test failed with exception:";
	private static final String TEST_FAILED_MESSAGE = "Test failed:";
	
	private Date updaterLaunchTime;
	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private String dateString = "";
	
	public Updater(String username, String password, int driverType) {
		this.username = username;
		this.password = password;
		this.driverType = driverType;
		// configure the driver
		switch(driverType){
		case IE_DRIVER:
			driver = new InternetExplorerDriver();
			break;
		case FIREFOX_DRIVER:
			driver = new FirefoxDriver();
			break;
		case HTML_UNIT_DRIVER:
		default:
			DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
			capabilities.setBrowserName("htmlunit");
			capabilities.setVersion("firefox");
			capabilities.setPlatform(Platform.ANY);
			capabilities.setJavascriptEnabled(true);

			driver = new HtmlUnitDriver(capabilities);
		}
	}
	
	/**
	 * login to the base url
	 */
	public void createNewSession(){
		
		driver.get(BASE_URL);
		
		// search for the login prompt
		WebElement loginPrompt = driver.findElement(By.xpath("//p[contains(text(), \""+TEXT_LOGIN_PROMPT+"\")]"));
		
		boolean sessionActive = loginPrompt == null;
		
		// logout if the session is active (let's make sure we aren't in someone else's account
		if(sessionActive){
			logout();
		}
		
		waitUntilPageContains(TEXT, TEXT_LOGIN_PROMPT, ONE_MINUTE);
		
		// let's do this from scratch
		WebElement usernameField = driver.findElement(By.xpath("//input[@type='text']"));
		usernameField.sendKeys(username);
		
		WebElement passwordField = driver.findElement(By.xpath("//input[@type='password']"));
		passwordField.sendKeys(password);
		
		WebElement loginButton = driver.findElement(By.xpath("//input[@value='Login']"));
		loginButton.click();
		
		boolean loginSuccessful = waitUntilPageContains(TEXT, "Select an Operation");
		System.out.println("Login successful? "+loginSuccessful);
	}
	
	private void goToResultDetailsPage(String suiteName, String runNumber){
		driver.get("http://172.16.10.114/atm/run/runSessions.mpl?suite="+suiteName+"&sessNum="+runNumber);
		
		// click on the run number
		String xpathExpression = "//a[contains(@href, \""+runNumber+"\") and contains(@href, \"sessionSummary\")]";
		WebElement runSelectorLink = driver.findElement(By.xpath(xpathExpression));
		String initialWindow = driver.getWindowHandle();
		// click on the run selection then wait for the popup window that contains the methods that were executed in the run
		Set<String> handlesBeforePopup = driver.getWindowHandles();
		runSelectorLink.click();
		String testCaseSelectorWindowHandle = getWindowHandleOfNewPopup(handlesBeforePopup);
		driver.switchTo().window(initialWindow);
		// switch to the popup window which will contain the methods that were run.
		driver.switchTo().window(testCaseSelectorWindowHandle);
		waitUntilPageContains(XPATH, "//table[@class='atm_dataTable']");
		
		// open the test case result editor by clicking the "Run This Test" link
//		String runXpath = XPATH_SUFFIX_RUN_TEST_LINK;
//		WebElement runThisTestLink = driver.findElement(By.xpath(runXpath));
//		handlesBeforePopup = driver.getWindowHandles();
//		runThisTestLink.click();
//		
//		// switch to the popup with the test case editor fields and fill them in.
//		String testEditorHandle = getWindowHandleOfNewPopup(handlesBeforePopup);
//		driver.switchTo().window(testEditorHandle); 
//
//		String currentUrl = driver.getCurrentUrl();
//		String testPath = currentUrl.substring(0, currentUrl.lastIndexOf(".dir"));
//		testPath = testPath.substring(0, testPath.lastIndexOf('/')+1);
//		sessionAuthTerminator = currentUrl.substring(currentUrl.lastIndexOf(".bts"));
//		
//		return testPath;
	}
	
	private String sessionAuthTerminator = "";
	
	private void editResult(String suiteName, String runNumber, List<TestCase> testCases){
		String XPATH_TEST_CLASS_PATH_SEPARATOR = ".dir/";
		String XPATH_TEST_METHOD_TERMINATOR = ".bts";
		String masterHandle = driver.getWindowHandle();


		driver.switchTo().window(masterHandle);
		goToResultDetailsPage(suiteName, runNumber);
		
		for(TestCase caseResult : testCases){
			
			// retrieve the class name and method name to to search the test case list
			String className = caseResult.getClassName();
			String methodName = caseResult.getMethodName(); 
			goToResultPageWithTest(className, methodName);

			String testCaseSelectorWindowHandle = driver.getWindowHandle();
			
			// this is the row that contains the links related to the test case that was run
			String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
			// this is the xpath to the column that contains 'fail' text in it
			String testCaseFailXpath = testCaseRowXpath+XPATH_SUFFIX_TEST_FAILED;
			
			waitUntilPageContains(XPATH, testCaseRowXpath);
			WebElement row = driver.findElement(By.xpath(testCaseRowXpath));
			boolean testFailedInApTest = false;
			try{
				WebElement status = row.findElement(By.xpath(".//td[4]"));
				testFailedInApTest = status.getText().contains("fail"); 
			} catch(Exception e){
				
			}
			// determine if it was a pass or fail.
			//boolean testFailedInApTest = driver.findElements(By.xpath(testCaseFailXpath)).size() != 0;
			
			if(testFailedInApTest){
				String editorWindowHandle = openTestCaseEditor(className, methodName);
				// close the selector window (if we don't it'll launch a modal that'll cause webdriver failure)
				driver.switchTo().window(testCaseSelectorWindowHandle).close();
				driver.switchTo().window(editorWindowHandle);
				// fill in the fields for this test method
				fillTestResultFields(caseResult);
			
//				
//				driver.switchTo().window(resultsPageHandle);
//				System.out.println(driver.getCurrentUrl());
				//driver.navigate().refresh();
				

				driver.switchTo().window(masterHandle);
				goToResultDetailsPage(suiteName, runNumber);
			} else {

			}
			
			
		} // loop end
	}
	private int getPageCount(){
		String header = "Display of page";
		waitUntilPageContains(TEXT, header);
		WebElement counter = driver.findElement(By.xpath("//p[contains(text(), \""+header+"\")]"));
		String displayCount = counter.getText();
		
		displayCount = displayCount.substring(0, displayCount.lastIndexOf("."));
		String maxStr = displayCount.substring(displayCount.lastIndexOf(" ")).trim();
		
		return Integer.parseInt(maxStr);
		
	}
	
	private int getCurrentPage(){
		String header = "Display of page";
		
		WebElement counter = driver.findElement(By.xpath("//p[contains(text(), \""+header+"\")]"));
		String displayCount = counter.getText();
		
		String currentPage = displayCount.substring(0, displayCount.lastIndexOf(" of "));
		currentPage = currentPage.substring(currentPage.lastIndexOf(" ")).trim();
		
		return Integer.parseInt(currentPage); 
		
	}

	private boolean goToResultPageWithTest(String className, String methodName){
		int pageCount = getPageCount();
		int currentPage = getCurrentPage();
		
		if(currentPage != 1){
			WebElement pageBox = driver.findElement(By.xpath("//input[@name = 'botPage']"));
			pageBox.sendKeys("1");
			pageBox.submit();
			waitUntilPageContains(TEXT, "1 of "+pageCount); 
			currentPage = 1;
		}
		// this is the row that contains the links related to the test case that was run
		String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
		boolean matchFound = false;
		while(!matchFound && (currentPage < pageCount)){
			// search the current page for the test case entry, only if it failed
			WebElement testCaseLink = elementExists(testCaseRowXpath);
			if(testCaseLink != null){
				String runXpath = testCaseRowXpath+XPATH_SUFFIX_RUN_TEST_LINK;
				//WebElement runThisTestLink = driver.findElement(By.xpath(runXpath));
				matchFound = true;
			} else {
				// not on the current page, check the next page
				currentPage++;
				WebElement pageBox = driver.findElement(By.xpath("//input[@name = 'botPage']"));
				pageBox.sendKeys(Integer.toString(currentPage));
				pageBox.submit();
				waitUntilPageContains(TEXT, currentPage+" of "+pageCount); 
			}
		}
		
		return matchFound;
	}
	private String openTestCaseEditor(String className, String methodName) {
		// this is the row that contains the links related to the test case that was run
		String testCaseRowXpath = "//tr[descendant::a[contains(.,'"+className+"') and contains(., '"+methodName+"')]]";
		// this is the xpath to the column that contains 'fail' text in it
		String testCaseFailXpath = testCaseRowXpath+XPATH_SUFFIX_TEST_FAILED;
		// this is the xpath to the column that contains 'pass' text in it
		String testCasePassXpath = testCaseRowXpath+XPATH_SUFFIX_TEST_PASSED;
		//String currentHandle = driver.getWindowHandle();
//		confirmAlert();
		String runXpath = testCaseRowXpath+XPATH_SUFFIX_RUN_TEST_LINK;
		WebElement runThisTestLink = driver.findElement(By.xpath(runXpath));
		Set<String> handlesBeforePopup = driver.getWindowHandles();
		runThisTestLink.click();
		// switch to the popup with the test case editor fields and fill them in.
		String testEditorHandle = getWindowHandleOfNewPopup(handlesBeforePopup);
		return testEditorHandle;
		
	}
	
	private boolean waitForAlert(long timeoutInMilliseconds){
		long startTime = System.currentTimeMillis();
		boolean alertShown = false;
		while(!alertShown && System.currentTimeMillis()-startTime<timeoutInMilliseconds){
			try{
				Alert alert = driver.switchTo().alert();
				return true;
			} catch (Exception e) {
				
			}
		}
		
		return false;
	}
	
	private void confirmAlert(String handle) {
		try{
		Alert alert = driver.switchTo().alert();
		System.err.println("found an alert with text: "+alert.getText());
		alert.dismiss();
		driver.switchTo().window(handle);
		} catch (Exception e){
		}
		
		
	}

	private WebElement elementExists(String xpath){
		try{
			return driver.findElement(By.xpath(xpath));
			
		} catch (Exception e){
			return null;
		}
	}

	private void fillTestResultFields(TestCase caseResult) {
		String testClass = caseResult.getClassName();
		String testMethod = caseResult.getMethodName();
		String runTimeInSeconds = caseResult.getTime();
		String error = caseResult.getTruncatedError(8);
		String failure = caseResult.getTruncatedFailure(8);
		boolean testPassed = caseResult.isTestPassed();
		String message = "";

		String resultSelectorXpath = "//select[@name='results']";
		String resultPass = "pass - The test passed";
		String resultFail = "fail - The test failed";
		
		String staffTimeSelectorXpath = "//select[@name='RUNDATA_exectimestaff']";
		
		
		String noteFieldXpath = "//textarea[@name='note']";
		String sendResultButtonXpath = "//input[@value='Send Result']";
		waitUntilPageContains(XPATH, resultSelectorXpath);
		// if the test passed, set the Result dropdown to "pass - The test passed", otherwise set it to "fail - The test failed"
		Select resultSelector = new Select(driver.findElement(By.xpath(resultSelectorXpath)));
		if(testPassed){
			resultSelector.selectByVisibleText(resultPass);
			message = PASS_MESSAGE;
		} else {
			resultSelector.selectByVisibleText(resultFail);
			message = FAIL_MESSAGE;
		}
		
		message = message+dateString+"\n"+EXECUTION_TIME_MESSAGE+runTimeInSeconds+"s.\n";
		
		if(error != null){
			message = message+TEST_FAILED_WITH_ERROR_MESSAGE+"\n"+error;
		}
		
		if(failure != null){
			message = message+TEST_FAILED_MESSAGE+"\n"+failure;
		}
		
		// set the Actual Staff Time to "2"
		Select staffTimeSelector = new Select(driver.findElement(By.xpath(staffTimeSelectorXpath)));
		staffTimeSelector.selectByVisibleText("2");
		
		// fill in the execution notes field
		WebElement noteField = driver.findElement(By.xpath(noteFieldXpath));
				
		noteField.sendKeys(message);
		
		// click on the "Send Result" button
		WebElement sendResultButton = driver.findElement(By.xpath(sendResultButtonXpath));
		sendResultButton.click();
		
	}

	private String getWindowHandleOfNewPopup(Set<String> initialWindowHandles) {
		return getWindowHandleOfNewPopup(initialWindowHandles, ONE_MINUTE);
	}
	
	private String getWindowHandleOfNewPopup(Set<String> initialWindowHandles, long timeout) {
		long startTime = System.currentTimeMillis();
		boolean popupFound = false;
		Set<String> handlesNow = null;
		while(System.currentTimeMillis()-startTime < timeout){
			handlesNow = driver.getWindowHandles();
			if(handlesNow.size() > initialWindowHandles.size()){
				popupFound = true;
				break;
			}
		}
		
		if(popupFound){
			// remove matching handles, and return the new one
			handlesNow.removeAll(initialWindowHandles);
			return (String)handlesNow.toArray()[0];
		} else {
			return null;
		}
	}

	public void closeSession(){
		driver.quit();
	}
	
	
	public void logout(){
		driver.get(BASE_URL);
		waitUntilPageContains(XPATH, XPATH_LOGOUT_LINK);
		WebElement logoutLink = driver.findElement(By.xpath(XPATH_LOGOUT_LINK));
		logoutLink.click();
	}
	
	@Deprecated
	public void update(String suiteName, String runNumber, File junitResultXml){
		updaterLaunchTime = new Date();
		dateString = dateFormat.format(updaterLaunchTime);
		// ensure that a new session is setup
		createNewSession();
		// retrieve the test case data
		List<TestCase> testCases = TestResultUnmarshaller.getJunitTestCaseResults(junitResultXml);
		// edit the results via webdriver
		editResult(suiteName, runNumber, testCases);
		closeSession();
	}
	
	
	private boolean waitUntilPageContains(int criteria, String text){
		return waitUntilPageContains(criteria, text, ONE_MINUTE);
	}
	
	private boolean waitUntilPageContains(int criteria, String text, long timeoutMillis){
		String source;
		long startTime = System.currentTimeMillis();
		Exception xpathNotFoundException;
		do{
			switch(criteria){
			case TEXT:
				try{
					source = driver.getPageSource();
					if(source.contains(text)){
						// found it, return
						return true;
					}
					// otherwise let's loop again
				} catch (WebDriverException e) {
					// if the error isn't an "a is null" error then this is legit.
//					if(!e.getMessage().contains("JavaScript Error: \"a is null\"")){
//						throw e;
//					}
				}
				break;
				
			case XPATH:
				try{
					if(driver.findElement(By.xpath(text)) != null){
						// found it, return
						return true;
					}
				} catch (Exception e) {
					xpathNotFoundException = e;
				}
				// otherwise let's loop again
				break;
			}
			
			// we only get this far if the criteria in the switch block isn't met
			try{
				driver.wait(ONE_SECOND);
			} catch (Exception e) {
			}
			
		} while(System.currentTimeMillis() - startTime < timeoutMillis);
		

		
		return false;
	}
}