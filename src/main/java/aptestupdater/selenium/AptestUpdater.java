package aptestupdater.selenium;

import aptestupdater.jaxb.TestCase;
import aptestupdater.jaxb.TestResultUnmarshaller;
import aptestupdater.selenium.page.AllSessionsListPage;
import aptestupdater.selenium.page.GenericLogoutPage;
import aptestupdater.selenium.page.LoginToApTestManagerPage;
import aptestupdater.selenium.page.PageObject;
import aptestupdater.selenium.page.SelectAnOperationPage;
import aptestupdater.selenium.page.SummaryOfSessionPage;
import aptestupdater.selenium.page.SummaryOfSessionPage.TestResult;
import aptestupdater.selenium.page.TestCasePage;
import aptestupdater.selenium.testbench.ApTestEnvironment;
import java.io.File;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AptestUpdater {

	
	private ApTestEnvironment apTestEnvironment;
	
	public static final String XPATH_TEST_CLASS_PATH_SEPARATOR = ".dir/";
	public static final String XPATH_TEST_METHOD_TERMINATOR = ".bts";
	
	public AptestUpdater(String browserName) {
		apTestEnvironment = new ApTestEnvironment(browserName);
	}

	public void updateAptest(String username, String password, String suiteName, String runNumber, File resultFile) {

		// navigate to the aptest server
		ApTestEnvironment.goToSystemUnderTestUrl();
		
		// first page should be login page... let's find out if there's an open session
		LoginToApTestManagerPage loginPage = new LoginToApTestManagerPage();
		loginPage.waitUntilDomIsLoaded();
		loginPage.populateElements();
		
		// we want to be at the login page... if we're not, then logout
		if(!loginPage.isCurrentPage()){
			logoutFromOpenSession();

			// we want to land on the login page
			ApTestEnvironment.goToSystemUnderTestUrl();

			loginPage.waitUntilDomIsLoaded();
			loginPage.populateElements();
		}
		
		// login here
		loginPage.login(username, password);
		
		// did we get in?
		SelectAnOperationPage selectAnOperationPage = new SelectAnOperationPage();
		selectAnOperationPage.waitUntilDomIsLoaded();
		if(!selectAnOperationPage.isCurrentPage()){
			loginPage.waitUntilDomIsLoaded();
			if(loginPage.isErrorMessagePresent()){
				System.err.println("Could not log in. Error message from ApTest was: \""+loginPage.getErrorMessage()+"\"");
			}
		} else {
			System.out.println("Successfully logged in  as: "+username);
			// retrieve the test case data
			List<TestCase> testCases = TestResultUnmarshaller.getJunitTestCaseResults(resultFile);
			// edit the results via webdriver
			editResult(suiteName, runNumber, testCases);
			selectAnOperationPage.setWindowFocus();
			closeSession();
		}
		
	}
	
	private void editResult(String suiteName, String runNumber, List<TestCase> testCases) {
		
		String XPATH_TEST_PAGE = ApTestEnvironment.getSystemUnderTestUrl().concat("/run/runSessions.mpl?suite="+suiteName+"&sessNum="+runNumber);
		
		// going to this url is quicker than navigating to the page.
		PageObject.getWebDriver().get(XPATH_TEST_PAGE);
		
		String runTestsMainWindow = PageObject.getWebDriver().getWindowHandle();
		
		// the window will have a list of all the run numbers
		AllSessionsListPage runTestsListPage = new AllSessionsListPage();
		runTestsListPage.waitUntilDomIsLoaded();
		runTestsListPage.setWindowFocus();	
		runTestsListPage.clickOnRunNumber(runNumber);
		
		SummaryOfSessionPage summaryOfSessionPage;
		
		// update the run
		for(TestCase caseResult : testCases){
			
			//Summary of Session page is shown
			summaryOfSessionPage = new SummaryOfSessionPage();
			summaryOfSessionPage.waitUntilDomIsLoaded();
			
			// retrieve the class name and method name to to search the test case list
			String className = caseResult.getClassName();
			String methodName = caseResult.getMethodName();
			
			boolean newResultPassed = caseResult.isTestPassed();
			
			SummaryOfSessionPage.TestResult result = summaryOfSessionPage.getTestResult(className, methodName);
			
			if(!newResultPassed && result == SummaryOfSessionPage.TestResult.PASS){
				System.err.println("[Skip] "+className+"."+methodName+"\n\tPassed in ApTest and failed in Eclipse.\n");
			} else if(result == SummaryOfSessionPage.TestResult.FAIL ||
					result == SummaryOfSessionPage.TestResult.UNTESTED){
				
				System.err.println("[Edit] "+className+"."+methodName+
						"\n\t"+ (result == SummaryOfSessionPage.TestResult.FAIL ? "Failed in ApTest" : "Was not run in ApTest")+
						" and "+(caseResult.isTestPassed() ? "passed in Eclipse" : "failed in Eclipse")+"\n");
				// update the test
				summaryOfSessionPage.clickRunThisTest(className, methodName);
				
				//enter data into popup
				TestCasePage testCasePage = new TestCasePage();
				testCasePage.waitUntilDomIsLoaded();
				
				summaryOfSessionPage.closeBrowser();
				testCasePage.setWindowFocus();
				
				testCasePage.fillTestResultFields(caseResult);
				testCasePage.clickSendResultButton();

				// WORKAROUND:
				// After clicking send result, an popup will appear that if dismissed, accepted or closed, it will cause
				// webdriver to crash. The Exception will be of the form
				// "Caused by: org.openqa.selenium.remote.ErrorHandler$UnknownServerException: '[JavaScript Error: "a is null""
				// To workaround this, a new session run page will have to be opened after a successful edit
				
				// return to the run number page and start over
				runTestsListPage.setWindowFocus();	
				runTestsListPage.clickOnRunNumber(runNumber);
				
			} else {
				System.err.println("[Skip] "+className+"."+methodName+"\n\tPassed in ApTest and passed in Eclipse.\n");
			}
			
			
		} // loop end
		
	}
	
	
	public void closeSession(){
		logoutFromOpenSession();
	}

	private void logoutFromOpenSession(){
		ApTestEnvironment.goToSystemUnderTestUrl();
		
		GenericLogoutPage logoutPage = new GenericLogoutPage();
		logoutPage.waitUntilDomIsLoaded();
		logoutPage.logout();
		
	}

	public void teardown() {
		ApTestEnvironment.teardown(true);
	}

}