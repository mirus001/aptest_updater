package aptestupdater.selenium.testbench;

import aptestupdater.selenium.page.PageObject;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class TestEnvironment {
	// the mobile site
	protected static String systemUnderTestUrl;
	
	public static final int FIREFOX = 0;
	public static final int CHROME = 1;
	
	public static final String FIREFOX_STRING = "firefox";
	public static final String CHROME_STRING = "chrome";
	
	private static int targetDevice = FIREFOX;
	
	protected TestEnvironment(String url){
		String command = System.getProperty("sun.java.command").toLowerCase();
		setupEnvironment(url, command);
	}
	
	protected TestEnvironment(String url, String browserName){
		setupEnvironment(url, browserName);
	}
	
	private void setupEnvironment(String url, String browserName){
		try{
			String command = System.getProperty("sun.java.command").toLowerCase();
			int device = getBrowserCodeFromCLI(browserName);
			if(device == -1) {
				device = FIREFOX;
			}
			RemoteWebDriver driver = createWebDriver(device);
			systemUnderTestUrl = url;
			PageObject.setBaseUrl(url);
			PageObject.setWebDriver(driver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	private int getBrowserCodeFromCLI(String command){
		
		if(command.contains(FIREFOX_STRING)){
			return FIREFOX;
		} else if(command.contains(CHROME_STRING)){
			return CHROME;
		} else {
			return -1;
		}
	}
	
	public static boolean isInitialized(){
		return systemUnderTestUrl != null;
	}
	
	private void setSystemUnderTest(String url){
		systemUnderTestUrl = url;
		PageObject.setBaseUrl(systemUnderTestUrl);
	}
	
	private RemoteWebDriver createWebDriver(int browser) throws Exception {
		RemoteWebDriver driver = null;
		
		driver = WebDriverFactory.getWebDriverForBrowser(browser);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		return driver;
	}

	public static int getTargetDevice(){
		return targetDevice;
	}

	public static void goToSystemUnderTestUrl() {
		if(!PageObject.getCurrentUrl().equals(systemUnderTestUrl)){
			PageObject.goToUrl(systemUnderTestUrl);
		}
		
	}

	public static final String getSystemUnderTestUrl(){
		return systemUnderTestUrl;
	}
	
	public static int getDevice(){
		return targetDevice;
	}
	public static void teardown(boolean clearCache) {
		if(PageObject.getWebDriver() != null){
			if(clearCache){
				PageObject.getWebDriver().manage().deleteAllCookies();
			}
			PageObject.getWebDriver().quit();
		}
	}

	public void restartBrowser(boolean clearCache) {
		PageObject.getWebDriver().quit();
		try{
			RemoteWebDriver driver = createWebDriver(targetDevice);
			if(clearCache){
				driver.manage().deleteAllCookies();
			}
			PageObject.setWebDriver(driver);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}