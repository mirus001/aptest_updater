package aptestupdater.selenium.testbench;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class WebDriverFactory {
	
	public static final int FIREFOX = 0;
	
	private WebDriverFactory(){
		// null
	}
	
	public static final RemoteWebDriver getWebDriverForBrowser(int browserType) throws MalformedURLException, Exception{
		RemoteWebDriver driver = null;
		switch(browserType){
		case FIREFOX:
			driver = getFirefoxBrowser();
			break;
		default:
			throw new Exception("WebDriverFactory not configured to create WebDriver for "+browserType);
		}
		
		return driver;
	}
	
	private static RemoteWebDriver getFirefoxBrowser()  throws MalformedURLException{
		RemoteWebDriver driver;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName("firefox");
		driver = new FirefoxDriver(capabilities);
        return driver;
	}
	
}