package aptestupdater;

import aptestupdater.cliparser.ArgumentParser;
import aptestupdater.jaxb.TestCase;
import aptestupdater.jaxb.TestResultUnmarshaller;
import aptestupdater.selenium.AptestUpdater;
import aptestupdater.selenium.Updater;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class AptestDriver {

	
	public static void main(String[] args) {
		AptestUpdater aptestUpdater = null;
		Updater updater = null;
		try{
			ArgumentParser parser = new ArgumentParser();
			parser.parse(args);
			String username = parser.getUsername();
			String password = parser.getPassword();
			String suiteName = parser.getSuite();
			String runNumber = parser.getRunNumber();
			String xmlLocation = parser.getXmlLocation();
			String browserType = parser.getBrowserType();
			boolean slowpokeRequested = parser.isLegacyRunRequested();
			
			File resultFile = new File(xmlLocation);
	
			if(!slowpokeRequested){
				if(browserType == null){
					browserType = "Firefox";
				}
				aptestUpdater = new AptestUpdater(browserType);
				aptestUpdater.updateAptest(username, password, suiteName, runNumber, resultFile);
				
			} else {
				updater = new Updater(username, password, Updater.FIREFOX_DRIVER);
				updater.update(suiteName, runNumber, resultFile);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} finally {
			if(aptestUpdater != null){
				aptestUpdater.teardown();
			}
		}
	}
	
	 protected static String runTerminalCommand(String command) throws IOException{
	    	Runtime runtime = Runtime.getRuntime();
	    	Process process = runtime.exec(command);
	    	
	    	BufferedReader terminalReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	    	String output = "";
	    	String lineRead = terminalReader.readLine();
	    	
	    	while(lineRead != null){
	    		output = output.concat(lineRead);
	    		lineRead = terminalReader.readLine();
	    	}
	    	
	    	return output;
	    }
	
}